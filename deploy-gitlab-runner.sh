#!/bin/bash

# Replace 'gitlab-local' with the actual IP address or hostname of your VM
REMOTE_HOST="YOUR_VMS_IP_OR_HOSTNAME_HERE"
REMOTE_USER="YOUR_USERNAME_HERE"

# Set the remote path where you want to copy the values.yaml file
REMOTE_PATH="/home/$REMOTE_USER/"

# Function to copy values.yaml to the remote VM
function copy_to_remote {
    scp ./values.yaml "$REMOTE_USER@$REMOTE_HOST:$REMOTE_PATH/gitlab-runner-values.yaml"
}

# Function to deploy GitLab Runner using Helm
function deploy_gitlab_runner {
    ssh "$REMOTE_USER@$REMOTE_HOST" "microk8s helm upgrade --namespace gitlab-runner gitlab-runner -f $REMOTE_PATH/gitlab-runner-values.yaml gitlab/gitlab-runner"
}

# Main script
copy_to_remote
deploy_gitlab_runner
