# K8s Gitlab Runner Setup
This repo simply gives the steps required to get a really simple gitlab runner up and running so that you can use pipelines in your gitlab.com projects. Yes gitlab.com has free runner credits, but you burn through them quick, and they have their limitations:
* hardware limitations (limited ram/cpu)
* docker in docker last time I played with it didn't play nice (that was years ago)
* inability to grab larger than 100mb artifacts off - granted you can use s3 or ftp or something to get around this limitation

## Prerequisites
* an old computer to use as a server, I personally advocate for installing a hypervisor like proxmox or ESXi. But feel free to go baremetal if you don't want to run anything else.
* if going baremetal, a usb stick for burning the ubuntu server iso to.


## Setup
1. Grab ubuntu server lts from [here](https://ubuntu.com/download/server)
2. either upload the iso file to your hypervisor, or use something like https://etcher.balena.io/ or https://rufus.ie/en/ to burn the iso to a usb stick.
3. Install ubuntu - follow the prompts. There's a few key bits that you need to enable, otherwise the defaults or common sense should get you there.
	1. ![](screenshots/1-ubuntu.png) - username and server hostname, useful to use the hostname in you ~/.ssh/config setup for ease of use
	2. ![](screenshots/2-ubuntu.png) - read up on ubuntu pro, its free for like 5 machines and autopatches vulnerabilities, takes a tiny amount of additional setup
	3. ![](screenshots/3-ubuntu.png) - don't forget to grab your ssh key from github and install openSSH - it adds the public key to your authorized_keys so you don't need to ssh in with a password
	4. ![](screenshots/4-ubuntu.png) - just install microk8s, we install docker manually as the snap gets all messed up otherwise.

4. Once installed ssh in using your github key.
```bash
## microk8s setup ##
# At this point you can see microk8s running with:
microk8s config
# However, it will state that it needs permissions, so run the following - these commands weren't tested with $USER, but if they don't work simply drop $USER in favour of your username
sudo usermod -a -G microk8s $USER
mkdir ~/.kube
sudo chown -R $USER ~/.kube
newgrp microk8s
# this should now print your kubectl configuration
microk8s config
# setup local kubeconfig and set the correct permissions
cd ~/.kube && microk8s config > config && chmod 600 ~/.kube/config
# print the nodes, which is the current machine!
microk8s kubectl get nodes

## docker setup ##
# Now, you don't need docker - but I can't work without it so lets install it
sudo snap refresh
sudo snap install docker

# setup docker groups and such (do docker ps and if it complains about docker sock and permssions do this)
# from here: https://askubuntu.com/questions/941816/permission-denied-when-running-docker-after-installing-it-as-a-snap
sudo addgroup --system docker
sudo adduser $USER docker
newgrp docker
sudo snap disable docker
sudo snap enable docker

```
5. in you gitlab.com, for your project or perhaps a group, got to settings,CI/CD and add a `new project runner` ![](screenshots/gitlab-runner-setup-1.png)
6. Fill in the details you want, and it will give you a runner token
6. back on your main machine, prepare your values.yaml file by modifying the values.yaml [here](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml)
	1. I have a working version thats based on what I personally run, called `values.example.yaml`. You will need to adjust or remove the additional volume mounted on line 318, but for getting state out of your runners I'd advise setting it up. additionally you will need to update the `runnerToken` value with the token from your gitlab.com account - more on that later.
7. Save your modified values as values.yaml to the root of this project.
8. next we need to install the gitlab runner helm chart!
```bash
### Install Gitlab Runner ###
# guide for setting up the gitlab runner, but will duplicate the stuff here
# https://docs.gitlab.com/runner/install/kubernetes.html
microk8s helm repo add gitlab https://charts.gitlab.io
microk8s helm repo update gitlab
microk8s helm search repo -l gitlab/gitlab-runner

# create suitable namespace for the runner
microk8s kubectl create namespace gitlab-runner
# scp across the valus.yaml to this machine - this next command runs on your laptop (not the vm). substitue gitlab-local for your VMs hostname or ip address. change /home/modes for your home dir on your VM.
scp ./values.yaml gitlab-local:/home/modes/gitlab-runner-values.yaml
microk8s helm install --namespace gitlab-runner gitlab-runner -f gitlab-runner-values.yaml gitlab/gitlab-runner

# make your state directory for your runner to have a volume to pass in (values.example.yaml line 318)
mkdir -p whatever/your/stateful/dir/was
# make it root as thats what docker runs as
chmod root:root whatever/your/stateful/dir/was
```
8. any other adjustments you want to make you can on your hostmachine, and simply run modify this `deploy-gitlab-runner.sh` and run it.
9. you should see the runner appear in your gitlab.com instance, be sure to enable it!